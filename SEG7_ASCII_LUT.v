module SEG7_ASCII_LUT	(	oSEG,iDIG	);
input	[7:0]	iDIG;
output	[6:0]	oSEG;
reg		[6:0]	oSEG;

always @(iDIG)
begin
		case(iDIG)
//		8'h00: oSEG = 7'b0000000;
//		8'h01: oSEG = ~7'b1111001;	// ---t----
//		8'h02: oSEG = 7'b0100100; 	// |	  |
//		8'h03: oSEG = 7'b0110000; 	// lt	 rt
//		8'h04: oSEG = 7'b0011001; 	// |	  |
//		8'h05: oSEG = 7'b0010010; 	// ---m----
//		8'h06: oSEG = 7'b0000010; 	// |	  |
//		8'h07: oSEG = 7'b1111000; 	// lb	 rb
//		8'h08: oSEG = 7'b0000000; 	// |	  |
//		8'h09: oSEG = 7'b0011000; 	// ---b----
//		8'h0a: oSEG = 7'b0001000;
//		8'h0b: oSEG = 7'b0000011;
//		8'h0c: oSEG = 7'b1000110;
//		8'h0d: oSEG = 7'b0100001;
//		8'h0e: oSEG = 7'b0000110;
//		8'h0f: oSEG = 7'b0001110;
//		8'h00: oSEG = 7'b1000000;
		8'h00: oSEG = ~7'h00; /* (space) */
		8'h01: oSEG = ~7'h86; /* ! */
		8'h02: oSEG = ~7'h22; /* " */
		8'h03: oSEG = ~7'h7E; /* # */
		8'h04: oSEG = ~7'h6D; /* $ */
		8'h05: oSEG = ~7'hD2; /* % */
		8'h06: oSEG = ~7'h46; /* & */
		8'h07: oSEG = ~7'h20; /* ' */
		8'h08: oSEG = ~7'h29; /* ( */
		8'h09: oSEG = ~7'h0B; /* ) */
		8'h0a: oSEG = ~7'h21; /* * */
		8'h0b: oSEG = ~7'h70; /* + */
		8'h0c: oSEG = ~7'h10; /* , */
		8'h0d: oSEG = ~7'h40; /* - */
		8'h0e: oSEG = ~7'h80; /* . */
		8'h0f: oSEG = ~7'h52; /* / */
		8'h10: oSEG = ~7'h3F; /* 0 */
		8'h11: oSEG = ~7'h06; /* 1 */
		8'h12: oSEG = ~7'h5B; /* 2 */
		8'h13: oSEG = ~7'h4F; /* 3 */
		8'h14: oSEG = ~7'h66; /* 4 */
		8'h15: oSEG = ~7'h6D; /* 5 */
		8'h16: oSEG = ~7'h7D; /* 6 */
		8'h17: oSEG = ~7'h07; /* 7 */
		8'h18: oSEG = ~7'h7F; /* 8 */
		8'h19: oSEG = ~7'h6F; /* 9 */
		8'h1a: oSEG = ~7'h09; /* : */
		8'h1b: oSEG = ~7'h0D; /* ; */
		8'h1c: oSEG = ~7'h61; /* < */
		8'h1d: oSEG = ~7'h48; /* = */
		8'h1e: oSEG = ~7'h43; /* > */
		8'h1f: oSEG = ~7'hD3; /* ? */
		8'h20: oSEG = ~7'h5F; /* @ */
		8'h21: oSEG = ~7'h77; /* A */
		8'h22: oSEG = ~7'h7C; /* B */
		8'h23: oSEG = ~7'h39; /* C */
		8'h24: oSEG = ~7'h5E; /* D */
		8'h25: oSEG = ~7'h79; /* E */
		8'h26: oSEG = ~7'h71; /* F */
		8'h27: oSEG = ~7'h3D; /* G */
		8'h28: oSEG = ~7'h76; /* H */
		8'h29: oSEG = ~7'h30; /* I */
		8'h2a: oSEG = ~7'h1E; /* J */
		8'h2b: oSEG = ~7'h75; /* K */
		8'h2c: oSEG = ~7'h38; /* L */
		8'h2d: oSEG = ~7'h15; /* M */
		8'h2e: oSEG = ~7'h37; /* N */
		8'h2f: oSEG = ~7'h3F; /* O */
		8'h30: oSEG = ~7'h73; /* P */
		8'h31: oSEG = ~7'h6B; /* Q */
		8'h32: oSEG = ~7'h33; /* R */
		8'h33: oSEG = ~7'h6D; /* S */
		8'h34: oSEG = ~7'h78; /* T */
		8'h35: oSEG = ~7'h3E; /* U */
		8'h36: oSEG = ~7'h3E; /* V */
		8'h37: oSEG = ~7'h2A; /* W */
		8'h38: oSEG = ~7'h76; /* X */
		8'h39: oSEG = ~7'h6E; /* Y */
		8'h3A: oSEG = ~7'h5B; /* Z */
		8'h3B: oSEG = ~7'h39; /* [ */
		8'h3C: oSEG = ~7'h64; /* \ */
		8'h3D: oSEG = ~7'h0F; /* ] */
		8'h3E: oSEG = ~7'h23; /* ^ */
		8'h3F: oSEG = ~7'h08; /* _ */
		8'h40: oSEG = ~7'h02; /* ` */
		8'h41: oSEG = ~7'h5F; /* a */
		8'h42: oSEG = ~7'h7C; /* b */
		8'h43: oSEG = ~7'h58; /* c */
		8'h44: oSEG = ~7'h5E; /* d */
		8'h45: oSEG = ~7'h7B; /* e */
		8'h46: oSEG = ~7'h71; /* f */
		8'h47: oSEG = ~7'h6F; /* g */
		8'h48: oSEG = ~7'h74; /* h */
		8'h49: oSEG = ~7'h10; /* i */
		8'h4A: oSEG = ~7'h0C; /* j */
		8'h4B: oSEG = ~7'h75; /* k */
		8'h4C: oSEG = ~7'h30; /* l */
		8'h4D: oSEG = ~7'h14; /* m */
		8'h4E: oSEG = ~7'h54; /* n */
		8'h4F: oSEG = ~7'h5C; /* o */
		8'h50: oSEG = ~7'h73; /* p */
		8'h51: oSEG = ~7'h67; /* q */
		8'h52: oSEG = ~7'h50; /* r */
		8'h53: oSEG = ~7'h6D; /* s */
		8'h54: oSEG = ~7'h78; /* t */
		8'h55: oSEG = ~7'h1C; /* u */
		8'h56: oSEG = ~7'h1C; /* v */
		8'h57: oSEG = ~7'h14; /* w */
		8'h58: oSEG = ~7'h76; /* x */
		8'h59: oSEG = ~7'h6E; /* y */
		8'h5A: oSEG = ~7'h5B; /* z */
		8'h5B: oSEG = ~7'h46; /* { */
		8'h5C: oSEG = ~7'h30; /* | */
		8'h5D: oSEG = ~7'h70; /* } */
		8'h5E: oSEG = ~7'h01; /* ~ */
		8'h5F: oSEG = ~7'h00; /* (del) */
		endcase
end

endmodule
